# kubectl Cheat Sheet #

* Deploy file ke cluster

    ```
    kubectl --kubeconfig=belajar-k8s-do-kubeconfig.yaml apply -f 3-catalog-app.yml
    ```

* List resources yang ada di cluster

    ```
    kubectl --kubeconfig=belajar-k8s-do-kubeconfig.yaml get pvc,services,pods
    ```

* Menampilkan log output salah satu pod

    ```
    kubectl logs -f catalog-676cb47cfd-t8tk6
    ```

* Informasi pod

    ```
    kubectl --kubeconfig=belajar-k8s-do-kubeconfig.yaml describe pods
    ```

* Daftar label pod

    ```
    kubectl --kubeconfig=belajar-k8s-do-kubeconfig.yaml get pods --show-labels
    ```

* Undeploy

    ```
    kubectl --kubeconfig=belajar-k8s-do-kubeconfig.yaml delete -f 3-catalog-app.yml
    ```

# Horizontal Pod Autoscale #

1. Deploy aplikasi ke k8s cluster

    DigitalOcean:

    ```
    kubectl apply -f 1-persistent-volume-do.yml -f 2-catalog-db.yml -f 3-catalog-app.yml
    ```
    
    Minikube: 
    
    ```
    kubectl apply -f 1-persistent-volume-minikube.yml -f 2-catalog-db.yml -f 3-catalog-app.yml
    ```

2. Deploy `metrics-server` ke k8s cluster

    Untuk di DigitalOcean, instal langsung dari repositorynya

    ```
    kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
    ```
   
    Untuk di minikube, cukup aktifkan addons dan nyalakan tunnel

    ```
    minikube addons enable metrics-server
    minikube tunnel
    ```

3. Lihat nama deployment aplikasi web

    ```
    kubectl get deployments
    ```

4. Enable HPA untuk deployment `catalog-app`

    ```
    kubectl autoscale deployment catalog-app --cpu-percent=50 --min=1 --max=5
    ```

5. Lihat status HPA

    ```
    kubectl get hpa
    ```

6. Tambahkan load

    ```
    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://catalog-lb/api/product/; done"
    ```
   
    atau menggunakan aplikasi `ab`

    ```
    ab -c 5 -n 1000 -t 100000 http://127.0.0.1/api/product/
    ```

7. Cek jumlah replica

    ```
    kubectl get deployment catalog-app
    ```

# Referensi #

* https://kubernetes.io/docs/reference/kubectl/cheatsheet/