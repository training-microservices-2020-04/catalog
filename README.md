## Membuat Docker Image ##

1. Build dulu aplikasinya, supaya menghasilkan file `target/*jar`

    ```
    mvn clean package -DskipTests
    ```

2. Build docker image

    ```
    docker build -t endymuhardin/catalog .
    ```

## Menjalankan aplikasi ##

1. Jalankan dulu database Postgresql 

    ```
    docker run -p 5432:5432 --name dbcatalog -e POSTGRES_USER=catalog -e POSTGRES_PASSWORD=catalog123 -e POSTGRES_DB=catalogdb postgres
    ```
   
2. Ubah konfigurasi, host database dikeluarkan menjadi variabel

    ```
    spring.datasource.url=jdbc:postgresql://${DBHOST}/catalogdb
    ```

3. Temukan alamat IP laptop

    ```
    ifconfig 
    en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
        options=400<CHANNEL_IO>
        ether f4:0f:24:2d:5a:6a 
        inet6 fe80::100e:817e:981:9fb7%en0 prefixlen 64 secured scopeid 0x5 
        inet 192.168.100.9 netmask 0xffffff00 broadcast 192.168.100.255
        nd6 options=201<PERFORMNUD,DAD>
        media: autoselect
        status: active
    ```

4. Jalankan aplikasi

    ```
    docker run -p 10001:10001 --name catalog -e DBHOST=192.168.100.9 -e SPRING_PROFILES_ACTIVE=dockerlocal  endymuhardin/catalog
    ```

## Connect ke database postgresql dalam container ##

1. Login dulu ke container

    ```
    docker exec -it <id-container> bash
    ```

2. Connect ke database

    ```dockerfile
    psql -h localhost -U catalog catalogdb
    ```
   
3. Jalankan query

    ```sql
    insert into product (id, code, name, price) values
      ('p001', 'P-001', 'Product 001', 100001);
    ```

## Menjalankan aplikasi dengan docker compose ##

1. Build dulu projectnya

    ```
    mvn clean package -DskipTests
    ```

2. Jalankan `docker-compose`

    ```
    docker-compose up
    ```

3. Browse ke `http://localhost:12345/api/product/`

## Upload image ke docker hub ##

1. Buat tag untuk image yang sudah dibuat

    ```
    docker image tag endymuhardin/catalog endymuhardin/catalog:0.0.1-SNAPSHOT
    ```

2. Login ke Docker Hub

    ```
    docker login                                                                        362ms  Thu Jan 14 11:04:57 2021
      Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
      Username: endymuhardin
      Password:
      Login Succeeded
    ```

3. Push / upload ke docker hub

    ```
    docker push --all-tags endymuhardin/catalog
    ```
   
    Outputnya seperti ini
   
    ```
    The push refers to repository [docker.io/endymuhardin/catalog]
      afa2c52bbe4e: Pushed
      4d280e5aa483: Pushed
      735b38a32f4c: Pushed
      edd61588d126: Mounted from library/openjdk
      9b9b7f3d56a0: Pushed
      f1b5933fe4b5: Mounted from library/openjdk
      0.0.1-SNAPSHOT: digest: sha256:880b53c181c70e42ef6d01581605d7c179f2b995ae836b2c88ea66ddf135ae05 size: 1575
      afa2c52bbe4e: Layer already exists
      4d280e5aa483: Layer already exists
      735b38a32f4c: Layer already exists
      edd61588d126: Layer already exists
      9b9b7f3d56a0: Layer already exists
      f1b5933fe4b5: Layer already exists
      latest: digest: sha256:880b53c181c70e42ef6d01581605d7c179f2b995ae836b2c88ea66ddf135ae05 size: 1575
    ```